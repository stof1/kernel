<?php 
namespace DarioRieke\Kernel\Event;

use DarioRieke\Kernel\KernelInterface;
use Psr\Http\Message\ServerRequestInterface;;

/**
 * KernelEvent
 */
class KernelEvent {
	/**
	 * @var KernelInterface
	 */
	private $kernel;

	/**
	 * @var ServerRequestInterface
	 */
	private $request;

	/**
	 * create new Kernel Event
	 * @param KernelInterface         $kernel  
	 * @param ServerRequestInterface  $request 
	 */
	public function __construct(KernelInterface $kernel, ServerRequestInterface $request) {
		$this->kernel = $kernel;
		$this->request = $request;
	}

	/**
	 * returns the current Request
	 * @return Request
	 */
	public function getRequest(): ServerRequestInterface {
		return $this->request;
	}

	/**
	 * get the Kernel which handles the Request
	 * @return KernelInterface
	 */
	public function getKernel(): KernelInterface {
		return $this->kernel;
	}
}
?>