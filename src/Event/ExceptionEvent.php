<?php 
namespace DarioRieke\Kernel\Event;

use DarioRieke\Kernel\Event\RequestEvent;
use DarioRieke\Kernel\KernelInterface;
use Psr\Http\Message\ServerRequestInterface;


/**
 * ExceptionEvent
 */
final class ExceptionEvent extends RequestEvent {

	/**
	 * @var \Exception
	 */
	private $exception;

	/**
	 * create an ExceptionEvent
	 * @param HttpKernelInterface      $kernel      
	 * @param ServerRequestInterface   $request      
	 * @param \Exception               $e           
	 */
	public function __construct(KernelInterface $kernel, ServerRequestInterface $request, \Exception $e)
	{
	    parent::__construct($kernel, $request);
	    $this->setException($e);
	}

	/**
	 * return the exception
	 * @return \Exception
	 */
	public function getException(): \Exception {
		return $this->exception;
	}
	/**
	 * set the exception
	 * @param \Exception $e
	 */
	public function setException(\Exception $e) {
		$this->exception = $e;
	}
}
?>