<?php 
namespace DarioRieke\Kernel\Event;

/**
 * KernelEvents
 * class holding the names for Kernel Events which can be dispatched and subscribed
 */
final class KernelEvents {
	/**
	 * use this Event to send a Response when a Request comes in
	 * Event gets dispatched when Request comes in
	 */
	const REQUEST = 'kernel.request';

	/**
	 * use this Event to handle uncaught Exceptions
	 * gets called if an uncaught exception occurs
	 */
	const EXCEPTION = 'kernel.exception';

	/**
	 * this Event gets called if a controller was found
	 * use it to change the controller
	 */
	const CONTROLLER = 'kernel.controller';

	/**
	 * use this Event to change the arguments passed to the controller
	 * gets called after the arguments are resolved
	 */
	const CONTROLLER_ARGUMENTS = 'kernel.controller_arguments';

	/**
	 * this Event gets dispatched if a Response has been created
	 * allows you to change the response before returning it to the client
	 */
	const RESPONSE = 'kernel.response';

	/**
	 * the view event is dispatched when a controller does not return a response 
	 * to turn the return value into a response
	 */
	const VIEW = 'kernel.view';

	/**
	 * gets called after the Response has been send
	 * typically to clean up the app state or execute heavy or time intense operations
	 */
	const FINISH = 'kernel.finish';
}
?>