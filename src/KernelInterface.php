<?php 
namespace DarioRieke\Kernel;

use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Kernel Interface
 */
interface KernelInterface extends RequestHandlerInterface {

} 
?>