<?php 
namespace DarioRieke\Kernel\Exception;

use DarioRieke\Kernel\Exception\KernelExceptionInterface;

class NoResponseException extends \LogicException implements KernelExceptionInterface {

}  

?>