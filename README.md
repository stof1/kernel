# Kernel

An event based PSR-15 RequestHandlerInterface to handle PSR-7 ServerRequests by returning a PSR-7 Response 

## Installation
install via composer

```
    "require": {
        "dariorieke/kernel": "dev-master"
    }
```

## Running tests

run tests with the following command

```
./vendor/bin/phpunit .\tests
```

## Usage
To construct the kernel, you have to provide:
- a [DarioRieke\EventDispatcher\EventDispatcherInterface](https://gitlab.com/dario.rieke/eventdispatcher) implementation,
- a [DarioRieke\Router\RouterInterface](https://gitlab.com/dario.rieke/router) implementation,
- a [DarioRieke\CallableResolver\CallableResolverInterface](https://gitlab.com/dario.rieke/callableresolver) 
- and a [DarioRieke\CallableResolver\ArgumentResolverInterface](https://gitlab.com/dario.rieke/callableresolver) implementation

the linked repos include the interface as well as a basic implementation.

```php
use DarioRieke\Kernel\Kernel;

$kernel = new Kernel(
    $dispatcher
    $router
    $callableResolver,
    $argumentResolver 
);

$psr7Response = $kernel->handle($psr7Request);

```

## Event based response handling

The kernel uses the provided EventDispatcher to dispatch different events to handle the response generation.
The following steps show the strategie which is used to generate the response:

### 1. incoming request 
When the request enters the kernel, the first event dispatched via the EventDispatcher is the `DarioRieke\Kernel\Event\RequestEvent` event. 
You can return a response at this point of the kernels workflow by calling the RequestEvent's `setResponse` method from an event handler. 

### 2. matching a route / resolving the controller
The second step is to determine the "controller" to handle the request, this is done via the `RouterInterface`.
Then the `CallableResolver` will resolve your controller and return an actual callable.
When the callable has been resolved, the `DarioRieke\Kernel\Event\ControllerEvent` event will be dispatched.
This event allows you to change the controller via the events `setController` method before it is actually called.

### 3. Resolving the controller arguments
Now that we have an actual callable, it probably needs some arguments too. This task is delegated to the `ArgumentResolverInterface`.
When the arguments have been successfully resolved, the `DarioRieke\Kernel\Event\ControllerArgumentsEvent` will be dispatched.
To alter the arguments passed to the controller, use the events `setArguments` method.

### 4. Calling the controller
The controller will now be called with the provided arguments. Typically the controller should return a PSR-7 `ResponseInterface`.
If that is the case, the `DarioRieke\Kernel\Event\ResponseEvent` will be dispatched which lets you alter the response right before it is returned via the events `setResponse`  method.

If the controller does not return a `ResponseInterface`, an optional event will be dispatched, the `DarioRieke\Kernel\Event\ViewEvent`. This event is responsible for turning the controllers return value into an actual `ResponseInterface` via the events `setResponse` method.

### Handling exceptions
If something goes wrong and an uncaught `\Exception` occurs, the `DarioRieke\Kernel\Event\ExceptionEvent` will be dispatched.
This way you can turn the exception into a Response to display the error, again via the events `setResponse` method.
